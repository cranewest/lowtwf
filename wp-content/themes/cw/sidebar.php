<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area small-12 medium-4 large-4 columns" role="complementary">

		<?php 	
				//adjusting the query
				$args = array(
					'post_type' => 'promos',
					'posts_per_page' => -1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					$count=0;
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
						$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
				
						$loc = get_post_meta( $post->ID, '_cwmb_location', true);

						if ($loc=='side' && $count==0){
							if ($link){
								echo'<a target="_blank" href="'.$link.'">'; 
								?><img class="promo" src="<? echo $img_src; ?>"> <?
								echo'</a>';

							} 
							else{
								?><img class="promo" src="<? echo $img_src; ?>"> <?
							}
							$count++;
						}
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>

		<div class="sidebar_widget">
			<div class="sidebar_contact_head">
				<h5><img src="<?php bloginfo('template_directory'); ?>/img/i_envelope.png">  &nbsp;&nbsp; Contact Us</h5>
			</div>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
		<div class="sidebar_call f_right">
			<h5 class="top_call">or Give Us a Call</h5>
			<a class="top_phone"href="tel:9407235698"><h4>940.723.LowT</h4></a>
			<h6 class="top_num">5 6 9 8 </h6>
		</div>
	</aside>

