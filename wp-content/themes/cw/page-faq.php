<?php
/* Template Name: FAQ Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="page_title_bg">
		<div class="row">
			<?echo '<h2 class="page-title">'.get_the_title().'</h2>';?>
		</div>
	</div>

	<div class="main row" role="main">

		<div class="small-12 medium-8 large-8 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						//echo '<h2 class="page-title">'.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>

			<ul class="accordion" data-accordion>
			  <li class="accordion-navigation">
			    <a href="#panel1a">Accordion 1</a>
			    <div id="panel1a" class="content active">
			      Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			    </div>
			  </li>
			  <li class="accordion-navigation">
			    <a href="#panel2a">Accordion 2</a>
			    <div id="panel2a" class="content">
			      Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			    </div>
			  </li>
			  <li class="accordion-navigation">
			    <a href="#panel3a">Accordion 3</a>
			    <div id="panel3a" class="content">
			      Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			    </div>
			  </li>
			</ul>



		</div>
		

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>