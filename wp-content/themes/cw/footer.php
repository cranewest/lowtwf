<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

<div class="footer_area1">

	<div class="row">
		<?php 	
			//adjusting the query
			$args = array(
				'post_type' => 'promos',
				'posts_per_page' => -1,
				'orderby' => 'menu_order'
			);

			// The Query
			$latest_post = new WP_Query( $args );
			// The Loop
			if ( $latest_post->have_posts() ) 
			{
				$count=0;
				while ( $latest_post->have_posts() ) 
				{	
					$latest_post->the_post();
					$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
					$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
			
					$loc = get_post_meta( $post->ID, '_cwmb_location', true);

					echo '<div class="small-12 medium-4 large-4 columns">';
					if ($loc=='foot' && $count<=2){
						if ($link){
							echo'<a target="_blank" href="'.$link.'">'; 
							?><img class="promo" src="<? echo $img_src; ?>"> <?
							echo'</a>';
							
						} 
						else{
							?><img class="promo" src="<? echo $img_src; ?>"> <?
						}
						$count++;
					}
					echo '</div>';

					
				}
			} 
			else 
			{
				// no posts do nothing
			}

			/* Restore original Post Data */
			wp_reset_postdata();
		?>
		<!-- <div class="small-12 medium-4 large-4 columns">
			<img class="center" src="<?php bloginfo('template_directory'); ?>/img/promos/8.png" >
		</div>
		<div class="small-12 medium-4 large-4 columns">
			<img class="center" src="<?php bloginfo('template_directory'); ?>/img/promos/9.png" >
		</div>
		<div class="small-12 medium-4 large-4 columns">
			<img class="center" src="<?php bloginfo('template_directory'); ?>/img/promos/7.png" >
		</div> -->
	</div>
	<div class="footer_area1_inner"></div>
	
</div>

<div class="footer_area">
	<footer role="contentinfo" class="row">
		<div class="small-12 medium-6 large-6 columns">
			<p></p>
			<p class="address">
				&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>,  All Rights Reserved. | <a href="/privacy-policy">Privacy Policy</a><br>
				<?$patsys_title = str_replace("\'", "'",cw_options_get_option( 'cwo_title' ));?>
				<strong><?echo $patsys_title;?><br></strong>
				<?echo cw_options_get_option( 'cwo_address1' );?><br>
				<?echo cw_options_get_option( 'cwo_city' );?>,
				<?echo cw_options_get_option( 'cwo_state' );?>
				<?echo cw_options_get_option( 'cwo_zip' );?><br>
				<?echo '<a href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?>
				<!-- <a href="tel:19408726284"><strong>1(940)872-6284</strong></a> -->
			</p>
		</div>
		<div class="small-12 medium-6 large-6 columns"><br>
			<a class="cw" href="http://crane-west.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/cw-light.png" alt="Site Powered by Crane | West" /></a>
			<!-- <a class="f_right" href="http://crane-west.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/cw-light.png" alt="Site Powered by Crane | West" /></a> -->
		</div>
	</footer>
</div>

	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<?php
		// must activate CW Options Page plugin for the line below to work
		$ga_code = cw_options_get_option('cwo_ga'); if( !empty($ga_code) ) {
	?>
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $ga_code; ?>', 'auto');
		ga('send', 'pageview');
	</script>
	<?php } ?>
</body>
</html>