<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php
		$favicon = cw_options_get_option( 'cwo_favicon' );
		if(!empty($favicon)) { echo '<link rel="shortcut icon" type="image/png" href="'.$favicon.'"/>'; }
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- <link href='http://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'> -->
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>


	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->
</head>

<body <?php body_class(); ?>>
	<header role="banner">
		<div class="row">

			<div class="small-12 medium-6 large-6 columns">
				<?php $logo_url = cw_options_get_option( 'cwo_logo' ); ?>
				<h1 class="logo">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<?php if(!empty($logo_url)) { ?>
							<img src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
						<?php } else {
							bloginfo( 'name' );
						} ?>
					</a>
				</h1>
			</div>

			<div class="small-12 medium-6 large-6 columns ">
				<div class="small-7 medium-8 large-9 columns top_info">
					<div class="f_right">
						<h6 class="top_call">Call Us</h6>
						<a class="top_phone"href="tel:9407235698"><h5>940.723.LowT</h5></a>
						<span class="top_num">5 6 9 8 </span>
						<h6 class="top_email"><a href="/contact/">Send us an email <img src="<?php bloginfo('template_directory'); ?>/img/i_envelope.png"></a></h6><br>
						<a class="top_facebook" href="https://www.facebook.com/lowtwellnesscenter"><img src="<?php bloginfo('template_directory'); ?>/img/25x25_facebook.png"></a>
					</div>
				</div>
				<div class="small-5 medium-4 large-3 columns">
					<img class="f_right sil_logo" src="<?php bloginfo('template_directory'); ?>/img/GuyWithLowT.png" alt="">
				</div>
				
			</div>

		</div>	

		<div class="nav-container contain-to-grid">
			<nav class="top-bar" data-topbar role="navigation">
				<ul class="title-area">
					<li class="name"></li>
					<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
				</ul>

				<section class="top-bar-section">
					<?php 
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => '',
								'menu_class' => 'menu',
								'depth' => 2,
								'fallback_cb' => 'wp_page_menu',
								'walker' => new Foundation_Walker_Nav_Menu()
							)
						);
					?>
				</section>
			</nav>
		</div>
	</header>

	<?php //get_template_part('content', 'slides'); ?>
