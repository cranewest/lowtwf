<?php
/* Template Name: Home Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="home_page_title_bg">
		<div class="row">
			<div class="small-12 medium-12 large-7 columns slide_content">
				<?php get_template_part('content', 'slides'); ?>
			</div>
			<div class="small-12 medium-12 large-5 columns slide_home_content">
				<?the_content();?>
			</div>
		</div>
	</div>
	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						//echo '<h2 class="page-title">'.get_the_title().'</h2>';
						//the_content();
						// comments_template( '', true );
					}
				}
			?>
			<br>
		</div>

		<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => 3,
					// 'orderby' => 'menu_order'
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
						echo'<div class = "small-12 medium-4 large-4 columns post_content"><br>';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><a target="_blank" href="<? echo $n_link; ?>"><strong><h4><? echo get_the_title(); ?></h4></strong></a><br><?
							}
							else{
								?><a href="<? the_permalink(); ?>"><strong><h4><? echo get_the_title(); ?></h4></strong></a><br><?
							}
							
			
							echo'<div class="">';
								// the_excerpt();
								the_content();
							echo'</div>';

							if($n_link)
							{
								?><br><a class="f_right" target="_blank" href="<?php echo $n_link; ?>">Read More</a><?
							}
							else{
								?><br><a class="f_right" href="<?php the_permalink(); ?>">Read More</a><?
							}
						echo'</div></div>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>