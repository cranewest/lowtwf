<?php
/**
 * FAQs Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_faqs_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'FAQs' ),
			'singular_name' => __( 'FAQs' ),
			'add_new' => __( 'Add New Question' ),
			'add_new_item' => __( 'Add New Question' ),
			'edit_item' => __( 'Edit Question' ),
			'new_item' => __( 'Add New Question' ),
			'view_item' => __( 'View Question' ),
			'search_items' => __( 'Search FAQs' ),
			'not_found' => __( 'No questions found' ),
			'not_found_in_trash' => __( 'No questions found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => false,
		'has_archive' => true,
		'menu_position' => 20,
		'supports' => array('title', 'page-attributes', 'editor'),
		'menu_icon' => 'dashicons-editor-help'
	);
	register_post_type('faqs',$field_args);
}
add_action( 'init', 'cw_cpp_faqs_init' );



/**
 * Tweaks to Featured Image on FAQs
 *
 * Moves Featured Image field from sidebar to main on faqs and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_faqs_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'faqs', 'side' );
// 	add_meta_box('postimagediv', __('Question Image'), 'post_thumbnail_meta_box', 'faqs', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_faqs_move_image_box');



/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to faqs.
 *
 * @since CW 1.0
 */
function cw_cpp_faqs_categories() {
	$field_args = array(
		'labels' => array(
			'name'              => _x( 'Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Categories' ),
			'all_items'         => __( 'All Categories' ),
			'parent_item'       => __( 'Parent Category' ),
			'parent_item_colon' => __( 'Parent Category:' ),
			'edit_item'         => __( 'Edit Category' ),
			'update_item'       => __( 'Update Category' ),
			'add_new_item'      => __( 'Add New Category' ),
			'new_item_name'     => __( 'New Category' ),
			'menu_name'         => __( 'Categories' ),
		),
		'hierarchical' => true
	);
	register_taxonomy( 'faqs_categories', 'faqs', $field_args );
}
add_action( 'init', 'cw_cpp_faqs_categories', 0 );