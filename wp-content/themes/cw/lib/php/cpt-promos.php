<?php
/**
 * Promos Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_promos_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Promos' ),
			'singular_name' => __( 'Promos' ),
			'add_new' => __( 'Add New Promo' ),
			'add_new_item' => __( 'Add New Promo' ),
			'edit_item' => __( 'Edit Promo' ),
			'new_item' => __( 'Add New Promo' ),
			'view_item' => __( 'View Promo' ),
			'search_items' => __( 'Search Promos' ),
			'not_found' => __( 'No promos found' ),
			'not_found_in_trash' => __( 'No promos found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'menu_position' => 20,
		'supports' => array('title'),
        'menu_icon' => 'dashicons-format-image'
	);
	register_post_type('promos',$field_args);
}
add_action( 'init', 'cw_cpp_promos_init' );



/**
 * Tweaks to Featured Image on Promos
 *
 * Moves Featured Image field from sidebar to main on promos and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_promos_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'promos', 'side' );
// 	add_meta_box('postimagediv', __('Promo Image'), 'post_thumbnail_meta_box', 'promos', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_promos_move_image_box');



/**
 * Add Locations to Custom Post Type
 *
 * This is just an example of adding locations to a custom post type.
 * To see in action just uncomment and it will add locations to promos.
 *
 * @since CW 1.0
 */
function cw_cpp_promos_locations() {
	$field_args = array(
		'labels' => array(
			'name'              => _x( 'Locations', 'taxonomy general name' ),
			'singular_name'     => _x( 'Location', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Locations' ),
			'all_items'         => __( 'All Locations' ),
			'parent_item'       => __( 'Parent Location' ),
			'parent_item_colon' => __( 'Parent Location:' ),
			'edit_item'         => __( 'Edit Location' ),
			'update_item'       => __( 'Update Location' ),
			'add_new_item'      => __( 'Add New Location' ),
			'new_item_name'     => __( 'New Location' ),
			'menu_name'         => __( 'Locations' ),
		),
		'hierarchical' => true
	);
	register_taxonomy( 'promos_locations', 'promos', $field_args );
}
add_action( 'init', 'cw_cpp_promos_locations', 0 );