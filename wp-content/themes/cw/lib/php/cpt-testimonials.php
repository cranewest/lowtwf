<?php
/**
 * Testimonials Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_testimonials_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Testimonials' ),
			'singular_name' => __( 'Testimonials' ),
			'add_new' => __( 'Add New Testimonial' ),
			'add_new_item' => __( 'Add New Testimonial' ),
			'edit_item' => __( 'Edit Testimonial' ),
			'new_item' => __( 'Add New Testimonial' ),
			'view_item' => __( 'View Testimonial' ),
			'search_items' => __( 'Search Testimonials' ),
			'not_found' => __( 'No testimonials found' ),
			'not_found_in_trash' => __( 'No testimonials found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'has_archive' => false,
		'menu_position' => 20,
		'supports' => array('title'),
        'menu_icon' => 'dashicons-admin-comments'
	);
	register_post_type('testimonials',$field_args);
}
add_action( 'init', 'cw_cpp_testimonials_init' );



/**
 * Tweaks to Featured Image on Testimonials
 *
 * Moves Featured Image field from sidebar to main on testimonials and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_testimonials_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'testimonials', 'side' );
// 	add_meta_box('postimagediv', __('Testimonial Image'), 'post_thumbnail_meta_box', 'testimonials', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_testimonials_move_image_box');



/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to testimonials.
 *
 * @since CW 1.0
 */
// function cw_cpp_testimonials_categories() {
// 	$field_args = array(
// 		'labels' => array(
// 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// 			'search_items'      => __( 'Search Categories' ),
// 			'all_items'         => __( 'All Categories' ),
// 			'parent_item'       => __( 'Parent Category' ),
// 			'parent_item_colon' => __( 'Parent Category:' ),
// 			'edit_item'         => __( 'Edit Category' ),
// 			'update_item'       => __( 'Update Category' ),
// 			'add_new_item'      => __( 'Add New Category' ),
// 			'new_item_name'     => __( 'New Category' ),
// 			'menu_name'         => __( 'Categories' ),
// 		),
// 		'hierarchical' => true
// 	);
// 	register_taxonomy( 'testimonials_categories', 'testimonials', $field_args );
// }
// add_action( 'init', 'cw_cpp_testimonials_categories', 0 );