<?php
/**
 * Links Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_links_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Links' ),
			'singular_name' => __( 'Links' ),
			'add_new' => __( 'Add New Link' ),
			'add_new_item' => __( 'Add New Link' ),
			'edit_item' => __( 'Edit Link' ),
			'new_item' => __( 'Add New Link' ),
			'view_item' => __( 'View Link' ),
			'search_items' => __( 'Search Links' ),
			'not_found' => __( 'No links found' ),
			'not_found_in_trash' => __( 'No links found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'has_archive' => true,
		'menu_position' => 20,
		'supports' => array('title'),
        'menu_icon' => 'dashicons-admin-links'
	);
	register_post_type('links',$field_args);
}
add_action( 'init', 'cw_cpp_links_init' );



/**
 * Tweaks to Featured Image on Links
 *
 * Moves Featured Image field from sidebar to main on links and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_links_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'links', 'side' );
// 	add_meta_box('postimagediv', __('Link Image'), 'post_thumbnail_meta_box', 'links', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_links_move_image_box');



/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to links.
 *
 * @since CW 1.0
 */
// function cw_cpp_links_categories() {
// 	$field_args = array(
// 		'labels' => array(
// 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// 			'search_items'      => __( 'Search Categories' ),
// 			'all_items'         => __( 'All Categories' ),
// 			'parent_item'       => __( 'Parent Category' ),
// 			'parent_item_colon' => __( 'Parent Category:' ),
// 			'edit_item'         => __( 'Edit Category' ),
// 			'update_item'       => __( 'Update Category' ),
// 			'add_new_item'      => __( 'Add New Category' ),
// 			'new_item_name'     => __( 'New Category' ),
// 			'menu_name'         => __( 'Categories' ),
// 		),
// 		'hierarchical' => true
// 	);
// 	register_taxonomy( 'links_categories', 'links', $field_args );
// }
// add_action( 'init', 'cw_cpp_links_categories', 0 );