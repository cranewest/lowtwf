<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="page_title_bg">
		<div class="row">
			<?echo '<h2 class="page-title">'.get_the_title().'</h2>';?>
		</div>
	</div>

	<div class="main row" role="main">
		<div class="small-12 medium-8 large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php 
					//get_template_part( 'content', get_post_format() ); 
					the_content();
				?>

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>